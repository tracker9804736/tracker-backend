# WEB TRACKER BACKEND

## Installation
Run `npm install` to install all package on this project

## Database
Create your `MongoDb` database and create .env file on your root based to .env.example
Don't forget to change .env config with your local config.

Run `prisma db push` to create your migrate

## Socket Io
Your socket listen on 3002 port. Your can change it on `worker.js` file.


## Development server
Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. 



