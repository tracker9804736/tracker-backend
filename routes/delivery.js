var express = require('express');
var router = express.Router();
const { PrismaClient, Prisma} = require('@prisma/client')
const prisma = new PrismaClient()


 router.post('/delivery',async function (req, res) {
     let data =  req.body;
     try {
         const check = await prisma.package.findUnique({
             where : {
                 package_id : data.package_id
             }
         });
         if (!check){
             return res.json({status : false, message : "Package not found !"})
         }

        let playload = {
            package_id : data.package_id,
            status : "Open"
         }

         const save = await prisma.delivery.create({data : playload});

         if (save){
             await prisma.package.update({
                 where:{package_id : data.package_id},
                 data : {
                     active_delivery_id : save.delivery_id
                 }
             })
             return res.json({status : true, message : "Delivery saved !"}).status(200);
         }

         return res.json({status : false, message : "Failed to save !"}).status(401);
     }catch (e) {
         return res.json({status : false, message : e.message}).status(401);
     }
});

 router.get('/delivery/:id', async function(req, res){
     let id = req.params.id;
     try {
         const data = await prisma.delivery.findUnique({
             where : {
                 delivery_id : id
             },
             include : {
                 package : true
             }
         })
         return res.json({status : "success", data : data});
     }catch (e) {

     }

 });

router.get('/delivery', async function(req, res){
    const data = await prisma.delivery.findMany({
        include : {
            package : true
        }
    })
    return res.json({status : "success", data : data});
});

router.put('/delivery/:id', async function(req, res){
    let id = req.params.id;
    let data = req.body;
    let playload = {
        pickup_time : data.pickup_time ?? new Date(data.pickup_time),
        start_time : data.start_time ??  new Date(data.start_time),
        end_time : data.end_time ?? new Date(data.end_time),
        location : data?.location,
        status : data?.status
    }
    try {
        const update = await prisma.delivery.update({
            where: {
                delivery_id: id
            },
            data: playload
        })
        return res.json({status: "success", message: "Delivery updated !", data: update});
    } catch (error) {
        res.json({status: "error", message: `Delivery with ID ${id} does not exist in the database`})
    }
});

router.delete(`/delivery/:id`, async (req, res) => {
    const { id } = req.params
    try {
        const data = await prisma.delivery.delete({
            where: {
                delivery_id: id,
            },
        })
        res.json({status: true, message: "Delivery deleted !"})
    }catch (e) {
        res.json({status: false, message: "Error server !"})
    }

})



module.exports = router;
