const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const {PrismaClient} = require("@prisma/client");
const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
cors : "*"
});
const prisma = new PrismaClient()

const Worker = () => {

    io.on("connection", (socket) => {
        socket.on('location_changed', async (data) => {
            console.log("location_changed !!!");
            try{
                let playload = {lng : data.lng.toString(), lat : data.lat.toString()}
               await  prisma.delivery.update({
                    where: {
                        delivery_id: data.delivery_id
                    },
                    data: {
                        location : playload
                    }
                });
                console.log(data.delivery_id)
                socket.broadcast.emit("delivery_updated"+data.delivery_id,playload)
            }catch (e) {
                console.log(e)
            }
        });

        socket.on('status_changed', async (message) => {
            console.log(message)
            switch (message.status) {
                case "picked-up" :
                    try{
                       await prisma.delivery.update({
                            where: {
                                delivery_id: message.id
                            },
                            data: {
                                pickup_time : new Date(),
                                status : "Picked_Up"
                            }
                        })
                    }catch (e) {
                        console.log(e)
                    }
                    break;

                case "in-transit" :

                    try{
                        await prisma.delivery.update({
                            where: {
                                delivery_id: message.id
                            },
                            data: {
                                start_time : new  Date(),
                                status : "In_transit"
                            }
                        })
                    }catch (e) {
                        console.log(e)
                    }
                    break;

                case "delivered" :
                    try{
                        await prisma.delivery.update({
                            where: {
                                delivery_id: message.id
                            },
                            data: {
                                end_time : new Date(),
                                status : "Delivered"
                            }
                        })
                    }catch (e) {
                        console.log(e)
                    }
                    break;

                case "failed" :
                    try{
                        await prisma.delivery.update({
                            where: {
                                delivery_id: message.id
                            },
                            data: {
                                end_time : new Date(),
                                status : "Failed"
                            }
                        })
                    }catch (e) {
                        console.log(e)
                    }
                    break;

            }
        });

    });
    io.listen(3002)

}
module.exports = {
    Ws: Worker
}