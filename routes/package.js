var express = require('express');
var router = express.Router();
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

 router.post('/package',async function (req, res) {
     let data =  req.body;
     try {
        let playload = {
             description : data?.description,
             from_address : data?.from_address,
             from_name : data?.from_name,
             from_location : data.from_location ? {lng : data.from_location.lng.toString(), lat : data.from_location.lat.toString()} : {lng: "0", lat : "0"},
             to_name : data?.to_name,
             to_address : data?.to_address,
             to_location : data.to_location ? {lng : data.to_location.lng.toString(), lat : data.to_location.lat.toString()} : {lng: "0", lat : "0"} ,
             weight : data?.weight,
             height : data?.height,
             depth : data?.depth
         }

         const save = await prisma.$transaction([prisma.package.create({data : playload})]);

         if (save){
             return res.json({status : true, message : "Package saved !"}).status(200);
         }

         return res.json({status : false, message : "Failed to save !"}).status(401);
     }catch (e) {
         return res.json(e);
     }
});

 router.get('/package/:id', async function(req, res){
     let id = req.params.id;
     const data = await prisma.package.findUnique({
         where : {
             package_id : id
         }
     })
     return res.json({status : "success", data : data});
 });

router.get('/package', async function(req, res){
    const data = await prisma.package.findMany()
    return res.json({status : "success", data : data});
});

router.put('/package/:id', async function(req, res){
    let id = req.params.id;
    let data = req.body;
    let playload = {
        description : data?.description,
        from_address : data?.from_address,
        from_name : data?.from_name,
        from_location : data.from_location ? {lng : data.from_location.lng.toString(), lat : data.from_location.lat.toString()} : {lng: "0", lat : "0"},
        to_name : data?.to_name,
        to_address : data?.to_address,
        to_location : data.to_location ? {lng : data.to_location.lng.toString(), lat : data.to_location.lat.toString()} : {lng: "0", lat : "0"},
        weight : data?.weight,
        height : data?.height,
        depth : data?.depth
    }
    try {
        const update = await prisma.package.update({
            where: {
                package_id: id
            },
            data: playload
        })
        return res.json({status: "success", message: "Package updated !", data: update});
    } catch (error) {
        res.json({status: "error", message: `Package with ID ${id} does not exist in the database`})
    }
});

router.delete(`/package/:id`, async (req, res) => {
    const { id } = req.params
    const data = await prisma.package.delete({
        where: {
            package_id: id,
        },
    })
    res.json({status: "success", message: "Package deleted !"})
})



module.exports = router;
