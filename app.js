var createError = require('http-errors');
var express = require('express');

var path = require('path');
var packageRouter = require('./routes/package');
var deliveryRouter = require('./routes/delivery');

var app = express();
var cors = require('cors');



const worker = require("./routes/worker");


// io.on('connection',  (socket) => {
//     //Update the location of a delivery
//     socket.on('location_changed', async (message) => {
//         try{
//             await prisma.delivery.update({
//                 where: {
//                     delivery_id: message.id
//                 },
//                 data: {
//                     location : {lng : message.lng, lat : message.lat}
//                 }
//             })
//         }catch (e) {
//
//         }
//     });
//
//     //Update the status of a delivery

//     socket.on('delivery_updated', (message) => {});
//
//     socket.on('disconnect', () => {});
// });
//
//

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.json());
app.use(cors());
app.use('/api', packageRouter);
app.use('/api', deliveryRouter);
worker.Ws()
;


module.exports = app;
